import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCategoryClientComponent } from './list-category-client.component';

describe('ListCategoryClientComponent', () => {
  let component: ListCategoryClientComponent;
  let fixture: ComponentFixture<ListCategoryClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCategoryClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCategoryClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
