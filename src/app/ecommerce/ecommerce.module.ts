import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCategoryClientComponent } from './list-category-client/list-category-client.component';
import { RouterModule } from '@angular/router';
const routes = [{ path: 'listCategory', component: ListCategoryClientComponent }];
@NgModule({
  declarations: [ListCategoryClientComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),

  ]
})
export class EcommerceModule { }
