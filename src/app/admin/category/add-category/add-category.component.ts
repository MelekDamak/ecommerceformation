import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Category } from 'src/app/model/category';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  catForm: FormGroup;
  constructor(private categoryService: CategoryService , private router: Router) { }

  ngOnInit() {
    this.catForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        ]),
      id: new FormControl('', [

        Validators.required,
       ])
    });
  }
  add() {
  const newCategory = new Category();
  newCategory.id = this.catForm.get('id').value;
  newCategory.name = this.catForm.get('name').value;

  this.categoryService.add(newCategory).subscribe(data =>  this.router.navigateByUrl('/listCategory'),
  err => alert('duplicated id'));
  }

}
