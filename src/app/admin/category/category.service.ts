import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/app/model/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }
url = 'http://localhost:3000/categorys';
  getAll(): Observable<any>
  {
    return this.http.get(this.url);
  }
  add(newCategory: Category): Observable<any> {
    return this.http.post(this.url, newCategory);

  }
  findById(id): Observable<any> {
   return this.http.get(this.url + '/' + id);
  }
  edit(category: Category){

    return this.http.put(this.url + '/' + category.id , category);
  }
  delete(category: Category) {
    return this.http.delete(this.url + '/' + category.id);
  }
}
