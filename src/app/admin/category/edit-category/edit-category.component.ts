import { Category } from 'src/app/model/category';
import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {
  id;
  category;
 load=false ;
 catForm: FormGroup;

  constructor(private activatedroute: ActivatedRoute, private categoryService: CategoryService , private route: Router) { }

  ngOnInit() {
    this.id = this.activatedroute.snapshot.paramMap.get('id');
    this.getCategory();

  }
  getCategory() {
    this.categoryService.findById(this.id).subscribe(
      result => {
        // Handle result
        console.log(result);
        this.catForm = new FormGroup({
            name: new FormControl(result.name, [
              Validators.required,
              Validators.minLength(4),
              ]),
            id: new FormControl(result.id, [
              Validators.required,
            ])
          });
        this.load = false;
       },
      error => {
        alert('not found');
      },
      () => {
        // 'onCompleted' callback.
        // No errors, route to new page here
        this.load = true;
      }
    );
  }
edit() {
  const category = new Category();
  category.id = this.catForm.get('id').value;
  category.name = this.catForm.get('name').value;
  this.categoryService.edit(category).subscribe(data =>
    this.route.navigateByUrl('/listCategory')
  )
}
}
