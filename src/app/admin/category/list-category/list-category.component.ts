import { CategoryService } from './../category.service';

import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

 
@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.css']
})
export class ListCategoryComponent implements OnInit {
  dataSource ;
  displayedColumns: string[] = ['id', 'name', 'actions'];
  selectedCategory;
  @ViewChild('dialogRef') dialogRef: TemplateRef<any>;

  constructor(public dialog: MatDialog , private categoryService: CategoryService) {}

  ngOnInit() {
  this.getAll();
  }
  getAll(){
    this.categoryService.getAll().subscribe(data => {
      this.dataSource = Array.from(data) ;
      console.log(data);
      });
  }
  editItem(row) {
    console.log(row);
  }
deleteItem(row) {
  this.selectedCategory = row;
  const dialogRef = this.dialog.open(this.dialogRef);
  dialogRef.afterClosed().subscribe(result => {
    console.log(`Dialog result: ${result}`);
  });

}
confirmDelete(){
console.log(this.selectedCategory);
this.categoryService.delete(this.selectedCategory).subscribe(data=>{
  this.getAll();
  alert('deleted');
});

}

}
