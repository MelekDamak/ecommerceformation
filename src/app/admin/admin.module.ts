import { SharedModule } from './../shared/shared.module';
import { CategoryService } from './category/category.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from '../nav-bar/nav-bar.component';
import { ListCategoryComponent } from './category/list-category/list-category.component';
import { DialogComponent } from '../dialog/dialog.component';
import { RouterModule } from '@angular/router';

const routes = [{ path: 'listCategory', component: ListCategoryComponent },
{ path: 'addCategory', component: ListCategoryComponent },
{ path: 'editCategory/:id', component: ListCategoryComponent }];

@NgModule({
  declarations: [ ListCategoryComponent,
    ListCategoryComponent,
    ListCategoryComponent,
    DialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [],
})
export class AdminModule { }
