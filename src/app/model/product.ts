import { Category } from './category';
export class Product {
    id: number;
    description: string;
    price: number;
    quantity: number;
    image: string;
    category: Category;

  }
